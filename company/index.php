<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Компания | ООО «АПК Астраханский»");
$dataObj = new bi_data();
$pictures = $dataObj->get_pictures('company');
$BoxesArr = $dataObj->getIBlockData(27);
$titlePicture = "";
$content_photo = "";
if(isset($pictures[0])){
    $titlePicture = $pictures[0]['PREVIEW_PICTURE'];
}else{
    $titlePicture = '';
}
if(isset($pictures[1])){
    $content_photo = $pictures[1]['PREVIEW_PICTURE'];
}else{
    $content_photo = '';
}
//p($_SERVER);
$server = "http://".$_SERVER['HTTP_HOST'];
$url = $server."/local/templates/BI_apk/include_areas/company/text4_block.php";

$SEO = new mySEO();

$desc = $SEO::GetDescriptionFromFile($url);

if($APPLICATION->GetDirProperty("description")=="АПК Астраханский"||$APPLICATION->GetDirProperty("description")=="") {
    $APPLICATION->SetPageProperty("ogdescription", $desc);
    $APPLICATION->SetPageProperty("description", $desc);
}

?>
    <div class="app-body">
        <main class="app-content">
            <div class="about about--content">
                <div class="about__inner">
                    <div class="about__topline">
                        <div class="about__topline-layout about__topline-layout--content">
                            <div class="h1 about__topline-title" data-aos-duration="1000" data-aos="fade-up">
                                <?$APPLICATION->IncludeFile(
                                    '/local/templates/BI_apk/include_areas/company/tittle_block.php',
                                    Array(),
                                    Array("MODE"=>"html")
                                );?>
                            </div>
                        </div>
                        <div class="about__topline-layout about__topline-layout--img">
                            <div class="about__topline-img">
                                <div class="about__topline-img-parallax">
                                    <img src="<?=$titlePicture?>" alt="" data-stellar-ratio="0.5"></div>
                                   
                                </div>
                            </div>
                        </div>

                   <!-- <div class="about__content" data-aos-duration="1000" data-aos="fade-up">
                        <div class="about__content-topline">
                            
                            <span>
                                <?/*$APPLICATION->IncludeFile(
                                    '/local/templates/BI_apk/include_areas/company/sub_title_block.php',
                                    Array(),
                                    Array("MODE"=>"html")
                                );*/?>
							
						</span></div>
                        <div class="about__content-bottom">
                            <?/*$APPLICATION->IncludeFile(
                                '/local/templates/BI_apk/include_areas/company/text_block.php',
                                Array(),
                                Array("MODE"=>"html")
                            );*/?>
                            
                        </div>
                    </div>-->
                    <?if(!empty($BoxesArr)):?>
                <div class="about__items">
                    <?foreach ($BoxesArr as $item) {?>
                        <div class="about__item <?if($item['PROPS']['BLUE_BG']['VALUE']){?>about__item--blue<?}?>">
                            <div class="about__item-background">
                                <img src="<?=$item['PREVIEW_PICTURE']?>" alt="<?=$item['NAME']?>">
                            </div>
                            <div class="about__item-content">
                                <div class="about__item-title"><span><?=$item['NAME']?></span></div>
                                <div class="about__item-text">
                                    <?=$item['PREVIEW_TEXT']?>
                                </div>
                                <?if($item['PROPS']['VIDEO_LINK']['VALUE']!=""):?>
                                <a class="about__item-link" href="<?=$item['PROPS']['VIDEO_LINK']['VALUE']?>">
                                    <span class="about__item-link-icon">
                                        <span class="svg-icon svg-icon--play"><svg class="svg-icon__link"><use
                                                        xlink:href="#play"/></svg></span>
                                    </span>
                                    <span class="about__item-link-text">
                                        Смотреть видео
                                    </span>
                                </a>
                                <?endif;?>
                            </div>
                        </div>
                    <?}?>
                </div>
                <?endif;?>
                    <div class="about__content" data-aos-duration="1000" data-aos="fade-up">
                        <div class="about__content-topline"> <span>
                                <?$APPLICATION->IncludeFile(
                                    '/local/templates/BI_apk/include_areas/company/sub_text3_block.php',
                                    Array(),
                                    Array("MODE"=>"html")
                                );?>
						</span></div>
                        <div class="about__content-bottom">
                            <?$APPLICATION->IncludeFile(
                                '/local/templates/BI_apk/include_areas/company/text4_block.php',
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                           
                        </div>
                    </div>
                <?if($content_photo!=""):?>
                    <div class="about__banner">
                        <div class="about__banner-inner"><img src="<?=$content_photo?>" alt=""
                                                              data-stellar-ratio="0.5"></div>
                        <div class="about__banner-square" data-aos="zoom-in" data-aos-duration="1000"></div>
                    </div>
                    <?endif;?>
                    <div class="about__content" data-aos-duration="1000" data-aos="fade-up">
                        <div class="about__content-topline"> <span>
                                 <?$APPLICATION->IncludeFile(
                                     '/local/templates/BI_apk/include_areas/company/sub_text4_block.php',
                                     Array(),
                                     Array("MODE"=>"html")
                                 );?>
							
						</span></div>
                        <div class="about__content-bottom">
                            <?$APPLICATION->IncludeFile(
                                '/local/templates/BI_apk/include_areas/company/text5_block.php',
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                            
                        </div>
                    </div>
                    <div class="about__slider">
                        <div class="scale-slider scale-slider--about">
                            <div class="scale-slider__inner">
                                <?foreach ($dataObj->get_pictures('company',2, true) as $slider_item):?>
                                    <div class="scale-slider__item">
                                        <a class="scale-slider__item-img" href="<?=$slider_item['DETAIL_PICTURE']?>">
                                            <img src="<?=$slider_item['PREVIEW_PICTURE']?>" alt="<?=$slider_item['NAME']?>">
                                        </a>
                                        <div class="scale-slider__item-content"> <?=$slider_item['PREVIEW_TEXT']?></div>
                                    </div>
                                <?endforeach;?>
                            </div>
                        </div>
                        <!-- b:scale-slider -->
                    </div>
                </div>
            </div>
            <!-- b:about -->
        </main>
    </div>
    <div class="app-body__fake-footer"></div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>