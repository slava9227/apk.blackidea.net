<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Политика конфиденциальности | ООО «АПК Астраханский»");
$dataObj = new bi_data();
$pictures = $dataObj->get_pictures('politic');

$titlePicture = "";
if(isset($pictures[0])){
    $titlePicture = $pictures[0]['PREVIEW_PICTURE'];
}else{
    $titlePicture = '';
}
$server = "http://".$_SERVER['HTTP_HOST'];
$url = $server."/local/templates/BI_apk/include_areas/politics/text_block.php";

$SEO = new mySEO();

$desc = $SEO::GetDescriptionFromFile($url);
if($APPLICATION->GetDirProperty("description")=="АПК Астраханский"||$APPLICATION->GetDirProperty("description")=="") {
    $APPLICATION->SetPageProperty("ogdescription", $desc);
    $APPLICATION->SetPageProperty("description", $desc);
}

?>
    <div class="app-body">
        <main class="app-content">
            <div class="about about--content">
                <div class="about__inner">
                    <div class="about__topline">
                        <div class="about__topline-layout about__topline-layout--content">
                            <div class="h1 about__topline-title" data-aos-duration="1000" data-aos="fade-up">
                                <?$APPLICATION->IncludeFile(
                                    '/local/templates/BI_apk/include_areas/politics/tittle_block.php',
                                    Array(),
                                    Array("MODE"=>"html")
                                );?>
                            </div>
                        </div>
                        <div class="about__topline-layout about__topline-layout--img">
                            <div class="about__topline-img">
                                <div class="about__topline-img-parallax">
                                    <img src="<?=$titlePicture?>" alt="" data-stellar-ratio="0.5"></div>

                            </div>
                        </div>
                    </div>

                    <div class="about__content" data-aos-duration="1000" data-aos="fade-up">
                        <div class="about__content-topline">

                            <span>
                                <?$APPLICATION->IncludeFile(
                                    '/local/templates/BI_apk/include_areas/politics/sub_title_block.php',
                                    Array(),
                                    Array("MODE"=>"html")
                                );?>

						</span></div>
                        <div class="about__content-bottom">
                            <?$APPLICATION->IncludeFile(
                                '/local/templates/BI_apk/include_areas/politics/text_block.php',
                                Array(),
                                Array("MODE"=>"html")
                            );?>

                        </div>
                    </div>

                    <div class="about__slider">
                        <div class="scale-slider scale-slider--about">
                            <div class="scale-slider__inner">
                                <?foreach ($dataObj->get_pictures('politics',2, true) as $slider_item):?>
                                    <div class="scale-slider__item">
                                        <a class="scale-slider__item-img" href="<?=$slider_item['DETAIL_PICTURE']?>">
                                            <img src="<?=$slider_item['PREVIEW_PICTURE']?>" alt="<?=$slider_item['NAME']?>">
                                        </a>
                                        <div class="scale-slider__item-content"> <?=$slider_item['PREVIEW_TEXT']?></div>
                                    </div>
                                <?endforeach;?>
                            </div>
                        </div>
                        <!-- b:scale-slider -->
                    </div>
                </div>
            </div>
            <!-- b:about -->
        </main>
    </div>
    <div class="app-body__fake-footer"></div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>