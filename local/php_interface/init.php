<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/classes/bi_tools.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/classes/bi_data.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/classes/bi_news.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/classes/bi_contacts.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/classes/bi_production.php");
class mySEO{

    public static function GetDescriptionFromFile($fileURL){
        $text = strip_tags(file_get_contents($fileURL));
        $max_lengh = 160;
        if(mb_strlen($text, "UTF-8") > $max_lengh) {
            $text_cut = mb_substr($text, 0, $max_lengh, "UTF-8");
            $text_explode = explode(" ", $text_cut);

            unset($text_explode[count($text_explode) - 1]);

            $text_implode = implode(" ", $text_explode);

            return "ООО «АПК Астраханский» : " .$text_implode."...";
        } else {
            return "ООО «АПК Астраханский» : " .$text;
        }
    }

    public static function GetDescriptionFromText($text){
        $max_lengh = 160;
        $text = strip_tags($text);
        if(mb_strlen($text, "UTF-8") > $max_lengh) {
            $text_cut = mb_substr($text, 0, $max_lengh, "UTF-8");
            $text_explode = explode(" ", $text_cut);

            unset($text_explode[count($text_explode) - 1]);

            $text_implode = implode(" ", $text_explode);

            return "ООО «АПК Астраханский» : ".$text_implode."...";
        } else {
            return "ООО «АПК Астраханский» : " .$text;
        }
    }
}