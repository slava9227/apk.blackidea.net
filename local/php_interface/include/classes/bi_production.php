<?php

class bi_production
{

    public function __construct()
    {
    }

    public function getProductionTabs($filter = [], $page = 1, $IBLOCK_ID = 5)
    {
        $arFields = array();
        if (CModule::IncludeModule("iblock")) {
            $arSelect = Array("*");
            $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");

            if (!empty($filter)) {
                $arFilter = array_merge($arFilter, $filter);
            }

            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect,
                array('iNumPage' => $page, 'nPageSize' => 4));
            $key = 0;
            while ($ob = $res->GetNextElement()) {
                $arFields[$key] = $ob->GetFields();
                $arFields[$key]['PREVIEW_PICTURE'] = CFile::GetPath($arFields[$key]["PREVIEW_PICTURE"]);
                $arFields[$key]['DETAIL_PICTURE'] = CFile::GetPath($arFields[$key]["DETAIL_PICTURE"]);
                $arFields[$key]['PROPS'] = $ob->GetProperties();
                $arFields[$key]['SVG'] = '';

                if(isset($arFields[$key]['PROPS']['SVG']) && !empty($arFields[$key]['PROPS']['SVG'])){
                    $arFields[$key]['SVG'] = CFile::GetPath($arFields[$key]['PROPS']['SVG']['VALUE']);
                }

                $key++;
            }
        }
        return $arFields;
    }

}