<?php
/**
 * Created by PhpStorm.
 * User: aleksej
 * Date: 21.04.18
 * Time: 13:40
 */

class bi_contacts
{
    private $iblockId = 4;

    public function __construct()
    {
    }

    public function getContacts($filter = array(), $page = 1)
    {
        $arFields = array();
        if (CModule::IncludeModule("iblock")) {
            $arSelect = Array("ID", "IBLOCK_ID", "CODE", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*","PREVIEW_PICTURE","DETAIL_PICTURE","PREVIEW_TEXT", "DETAIL_TEXT");
            $arFilter = Array("IBLOCK_ID" => $this->iblockId, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");

            if(!empty($filter)) {
                $arFilter = array_merge($arFilter, $filter);
            }

            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect, array('iNumPage'=> $page, 'nPageSize'=> 4));
            $key = 0;
            while ($ob = $res->GetNextElement()) {
                $arFields[$key] = $ob->GetFields();
                $arFields[$key]['PREVIEW_PICTURE'] = CFile::GetPath($arFields[$key]["PREVIEW_PICTURE"]);
                $arFields[$key]['DETAIL_PICTURE'] = CFile::GetPath($arFields[$key]["DETAIL_PICTURE"]);
                $arFields[$key]['PROPS'] = $ob->GetProperties();

                $arFields = $arFields[0];
                break;
            }
        }
        return $arFields;
    }
}