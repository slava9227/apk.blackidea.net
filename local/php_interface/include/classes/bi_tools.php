<?php
/**
 * Created by PhpStorm.
 * User: maxharper
 * Date: 20.04.18
 * Time: 11:11
 */

/**
 * Дебаг
 * @param $val
 */
function p($val)
{
    echo "<pre>";
    print_r($val);
    echo "</pre>";
}


function pp($my_array)
{
    if (is_array($my_array)) {
        echo "<table border=1 cellspacing=0 cellpadding=3 width=100%>";
        echo '<tr><td colspan=2 style="background-color:#333333;"><strong><font color=white>ARRAY</font></strong></td></tr>';
        if(empty($my_array)){
            echo '<tr><td valign="top" style="width:40px;background-color:#F0F0F0;">';
            echo '<strong>' . 'Пустой :(' . "</strong></td><td>";
            echo "</td></tr>";
        }
        foreach ($my_array as $k => $v) {
            echo '<tr><td valign="top" style="width:40px;background-color:#F0F0F0;">';
            echo '<strong>' . $k . "</strong></td><td>";
            pp($v);
            echo "</td></tr>";
        }
        echo "</table>";
        return;
    }
    echo $my_array;
}

/**
 * Класс для различных инструментов
 * Class bi_tools
 */
class bi_tools
{
    public static function check_lang()
    {
        if (explode('/', $_SERVER['REQUEST_URI'])[1] == 'en') {
            return 'en';
        } else {
            return 'ru';
        }
    }
}