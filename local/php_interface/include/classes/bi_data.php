<?php
/**
 * Created by PhpStorm.
 * User: maxharper
 * Date: 20.04.18
 * Time: 15:28
 */

/**
 * Класс для работы с фото на сайте apk
 * Class bi_data
 */
class bi_data
{
    /**
     * Получаем выборку слайдов по символьному коду раздела в библиотеке
     * @param string $sectionCode
     * @return array
     */
    public function get_pictures($sectionCode = 'main', $IBLOCK_ID = 2, $slider = false){
        $arFields = array();
            if (CModule::IncludeModule("iblock")) {
                $arSelect = Array("ID", "NAME","PREVIEW_PICTURE","DETAIL_PICTURE","PREVIEW_TEXT", "DATE_ACTIVE_FROM");
                if($slider){
                    $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_CODE" => $sectionCode,"PROPERTY_SLIDER_VALUE" => "Да" );
                }
                else{
                    $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_CODE" => $sectionCode,"PROPERTY_SLIDER_VALUE" => "Нет" );
                }
                $res = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);
                $key = 0;
                while ($ob = $res->GetNextElement()) {
                    $arFields[$key] = $ob->GetFields();
                    $arFields[$key]['PREVIEW_PICTURE'] = CFile::GetPath($arFields[$key]["PREVIEW_PICTURE"]);
                    $arFields[$key]['DETAIL_PICTURE'] = CFile::GetPath($arFields[$key]["DETAIL_PICTURE"]);
                    $key++;
                }
            }
        return $arFields;
    }

    /**
     * Выбирает данные из инфоблока по фильтру, если указан
     * @param $iblock_id
     * @param array $filter
     * @param int $page
     * @return array
     */
    public function getIBlockData($iblock_id, $filter = [], $page = 1)
    {
        $arFields = array();
        if (CModule::IncludeModule("iblock")) {
            $arSelect = Array("ID", "IBLOCK_ID", "CODE", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*","PREVIEW_PICTURE",
                "DETAIL_PICTURE","PREVIEW_TEXT", "DETAIL_TEXT");
            $arFilter = Array("IBLOCK_ID" => $iblock_id, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");

            if (!empty($filter)) {
                $arFilter = array_merge($arFilter, $filter);
            }

            $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter,false,
                array("iNumPage"=>$page, "nPageSize"=> 100), $arSelect);
            $key = 0;
            while ($ob = $res->GetNextElement()) {
                $arFields[$key] = $ob->GetFields();
                $arFields[$key]['PREVIEW_PICTURE'] = CFile::GetPath($arFields[$key]["PREVIEW_PICTURE"]);
                $arFields[$key]['DETAIL_PICTURE'] = CFile::GetPath($arFields[$key]["DETAIL_PICTURE"]);
                $arFields[$key]['PROPS'] = $ob->GetProperties();
                $key++;
            }
        }
        return $arFields;
    }
}