<?php
/**
 * Created by PhpStorm.
 * User: maxharper
 * Date: 20.04.18
 * Time: 17:00
 */

/**
 * Класс работы с новостями
 * Class bi_news
 */
class bi_news
{
    public $IBLOCK_ID = 3;
    public $year;

    public function __construct($year = null){
        if($year == null ){
            $this->year = date('Y');
        }
        else{
            $this->year = $year;
        }
    }

    /**
     * Выборка новостей по фильтру, по умолчанию текущий год
     * @param array $filter
     * @param int $page
     * @return array
     */
    public function get_news($filter = array(), $page = 1, $onlyOneNwws = false){
        $arFields = array();
        if (CModule::IncludeModule("iblock")) {
            $arSelect = Array("ID", "IBLOCK_ID", "CODE", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*","PREVIEW_PICTURE","DETAIL_PICTURE","PREVIEW_TEXT", "DETAIL_TEXT");
            $arFilter = Array("IBLOCK_ID" => $this->IBLOCK_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_CODE" => $this->year);

            if(!empty($filter)) {
                $arFilter = array_merge($arFilter, $filter);
            }

            $res = CIBlockElement::GetList(Array("ORDER"=>"ASC"), $arFilter,false, array("iNumPage"=>$page, "nPageSize"=> 4), $arSelect);
            $key = 0;
            while ($ob = $res->GetNextElement()) {
                $arFields[$key] = $ob->GetFields();
                $arFields[$key]['PREVIEW_PICTURE'] = CFile::GetPath($arFields[$key]["PREVIEW_PICTURE"]);
                $arFields[$key]['DETAIL_PICTURE'] = CFile::GetPath($arFields[$key]["DETAIL_PICTURE"]);
                $arFields[$key]['PROPS'] = $ob->GetProperties();

                $key++;

                if($onlyOneNwws){
                    $arFields = $arFields[0];
                    break;
                }
            }
        }
        return $arFields;
    }

    /**
     * Получить список годов
     */
    public function get_years(){
        $result = array();
        if (CModule::IncludeModule("iblock")) {
            $arFilter = Array('IBLOCK_ID' => $this->IBLOCK_ID, 'GLOBAL_ACTIVE' => 'Y');
            $db_list = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, true);
            while ($ar_result = $db_list->GetNext()) {
                if($ar_result['ELEMENT_CNT']>0) {
                    $result[] = $ar_result['NAME'];
                }
            }
        }
        return $result;
    }

    /**
     * Получить количество новостей, по умолчанию текущий год
     * @param array $filter
     * @return int
     */
    public function total_count($filter = array()){
        $arFields = array();
        if (CModule::IncludeModule("iblock")) {
            $arSelect = Array("ID", "IBLOCK_ID", "NAME");
            $arFilter = Array("IBLOCK_ID" => $this->IBLOCK_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_CODE" => $this->year);
            if(!empty($filter)) {
                $arFilter = array_merge($arFilter, $filter);
            }
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            $count = 0;
            while ($ob = $res->GetNextElement()) {

                $count++;
            }
        }
        return $count;
    }

    public function get_micro_news_by_id($ID){
        $arFields = array();
        if (CModule::IncludeModule("iblock")) {
            $arSelect = Array("ID", "IBLOCK_ID", "CODE", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*","PREVIEW_PICTURE","DETAIL_PICTURE","PREVIEW_TEXT", "DETAIL_TEXT");
            $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $ID);

            $res = CIBlockElement::GetList(Array("ORDER"=>"ASC"), $arFilter,false, array("iNumPage"=>$page, "nPageSize"=> 4), $arSelect);
            $key = 0;
            while ($ob = $res->GetNextElement()) {
                $arFields[$key] = $ob->GetFields();
                $arFields[$key]['PREVIEW_PICTURE'] = CFile::GetPath($arFields[$key]["PREVIEW_PICTURE"]);
                $arFields[$key]['DETAIL_PICTURE'] = CFile::GetPath($arFields[$key]["DETAIL_PICTURE"]);
                $arFields[$key]['PROPS'] = $ob->GetProperties();
                $key++;
            }
        }
        return $arFields;
    }

}