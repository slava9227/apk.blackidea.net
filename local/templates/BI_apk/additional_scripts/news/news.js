$('.news__topline-tags-link').click(function () {
   var $this = $(this);
   $('.active.news__topline-tags-link').removeClass('active');
   $this.addClass('active');
   var Obj = {};
   Obj.SECTION_CODE = $this.html();
   Obj.TYPE = 'year';
    console.log(Obj);
    $.ajax({
        type: "POST",
        url: "/ajax/api/news_api.php",
        data: Obj,
        success: function (msg) {
            $('.news__content-inner').empty();
            $('.news__content-inner').append(msg);
            var count = $('.count-of-year').val();
            if(Number(count)>=4){
                $('.news__more').show();
                $('.news__more').attr('data-current-page',1);
            }
            else {
                $('.news__more').hide();
            }
        }
    });
});

$('.more-btn').click(function () {
    var $this = $(this);

    $this.addClass('active');
    var page = Number($('.news__more').attr('data-current-page'));
    page = page+1;
    $('.news__more').attr('data-current-page',page);
    var Obj = {};
    Obj.SECTION_CODE = $('.active.news__topline-tags-link').html();
    if(Obj.SECTION_CODE === undefined){
        Obj.SECTION_CODE = 2018;
    }

    Obj.TYPE = 'more';
    Obj.PAGE = page;
    if(page === Math.ceil(Number($('.news__more').attr('data-total-count')/4))){
        $('.news__more').hide();
    }
    console.log(Obj);
    $.ajax({
        type: "POST",
        url: "/ajax/api/news_api.php",
        data: Obj,
        success: function (msg) {
            $('.news__content-inner').append(msg);
            $(window).data('plugin_stellar').refresh();
        }
    });
});