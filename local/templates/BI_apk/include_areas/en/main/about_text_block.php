<p> Modern plant with two Italian lines with the capacity of 3 thousand tons of raw materials per day </p>
<p> 3000 Ha area for planting and growing tomatoes </p>
<p> Greenhouses are equipped with climate control, and automatic irrigation with area of 5.7 Ha </p>
<p> 150 units of modern agricultural machinery and accessories </p>
<p> Own communication system: steam boiler, water treatment system, irrigation
    system with diesel pumping stations, treatment facilities, power lines and
    gas supply </p>
<p> harvesting tomatoes with harvesters over 90 tonnes per Hectare </p>