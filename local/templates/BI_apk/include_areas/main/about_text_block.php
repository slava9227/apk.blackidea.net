<p>
</p>
<p>
	 Крупнейшее в России предприятие по производству томатной пасты, расположено на родине томатов - Астраханской области.
</p>
<p>
	 Комплекс представляет собой замкнутый цикл от выращивания рассады&nbsp;до производства томатной пасты.
</p>
<p>
	 В 2016 году между областным Правительством и ООО «АПК «Астраханский» был заключен инвестиционный договор о присвоении проекту статуса «особо важный инвестиционный проект».
</p>
<p>
	 На предприятии задействовано самое современное оборудование итальянской компании ROSSI CATELLI CFT SPA.
</p>
<p>
	 Производственные мощности позволяют удовлетворить более 20% спроса на продукцию на территории Российской Федерации.
</p>
<p>
	 Предприятие сертифицировано по международной системе FSSC 22000
</p>
 <br>
<p>
</p>