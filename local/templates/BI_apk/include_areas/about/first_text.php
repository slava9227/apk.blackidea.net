<p> Для качественного производства томатной пасты на предприятии установлено современное оборудование итальянской компании ROSSI CATELLI SPA, которая является признанным мировым лидером в области оборудования, предназначенного
    для переработки овощей и производства томатной пасты. </p>
<p> Производственные линии позволяют реализовать два основных технологических процесса ColdBreak и HotBreak, а также изменять ассортимент в зависимости от потребностей рынка. </p>
<p> Технология приготовления концентрата из томатов происходит путем нагрева измельченных томатов паром через теплообменник для лучшего отделения кожуры и семечек. </p>
