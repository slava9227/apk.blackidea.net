$(document).ready(function() {

    "use strict";

    /**
     * Возвращает функцию, которая не будет срабатывать, пока продолжает вызываться.
     * Она сработает только один раз через N миллисекунд после последнего вызова.
     * Если ей передан аргумент `immediate`, то она будет вызвана один раз сразу после
     * первого запуска.
     */
    function debounce(func, wait, immediate) {

        var timeout;

        return function() {
            var context = this,
                args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };

            var callNow = immediate && !timeout;

            clearTimeout(timeout);
            timeout = setTimeout(later, wait);

            if (callNow) func.apply(context, args);
        };
    };

    /**
     * определение версии IE 10 или 11
     *
     * @returns {Number} 10, 11, или 0 если это не IE
     */
    function GetIEVersion() {

        var sAgent = window.navigator.userAgent;
        var Idx = sAgent.indexOf("MSIE");

        // If IE, return version number.
        if (Idx > 0) {
            return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
        }

        // If IE 11 then look for Updated user agent string.
        else if ( !! navigator.userAgent.match(/Trident\/7\./)) {
            return 11;
        } else {
            return 0; //It is not IE
        }
    }
    
    var ieVersion = GetIEVersion();
    if(ieVersion){
        $('html').addClass('ie');
    }


    // Test via a getter in the options object to see if the passive property is accessed
    window.supportsPassive = false;

    try {

        var opts = Object.defineProperty({}, 'passive', {
            get: function() {
                window.supportsPassive = true;
            }
        });

        window.addEventListener("test", null, opts);
    } catch (e) {}

    /**
     * определение существования элемента на странице
     */
    $.exists = function(selector) {
        return ($(selector).length > 0);
    }


    ;
    (function() {
        var bgCover = $('[data-section-bg]');

        if (!$.exists(bgCover)) {
            return;
        }

        var cover = function() {
            var self = $(this);
            var selfBg = self.data('section-bg');

            if (selfBg) {
                self.css('background-image', 'url(' + selfBg + ')');
            }
        };

        $.each(bgCover, cover);
    })();
    if ($.exists('.app-form--partners')) {
        var form = $('.app-form--partners');
        var formPosition = form.offset().top - 400;
        var layout = form.find('.app-form__layout--form');
        var layout500 = layout.data('500');
        var layout700 = layout.data('700');
        var layout900 = layout.data('900');
        var block = form.find('.app-form__block');
        var block500 = block.data('500');
        var block700 = block.data('700');
        var block900 = block.data('900');
        var layoutImg = form.find('.app-form__layout--img');
        var layoutImg500 = layoutImg.data('500');
        var layoutImg700 = layoutImg.data('700');
        var layoutImg900 = layoutImg.data('900');
        layout.removeAttr('data-500').removeAttr('data-700').removeAttr('data-900').attr('data-' + formPosition, layout500).attr('data-' + (formPosition + 200), layout700).attr('data-' + (formPosition + 400), layout900);
        block.removeAttr('data-500').removeAttr('data-700').removeAttr('data-900').attr('data-' + formPosition, block500).attr('data-' + (formPosition + 200), block700).attr('data-' + (formPosition + 400), block900);
        layoutImg.removeAttr('data-500').removeAttr('data-700').removeAttr('data-900').attr('data-' + formPosition, layoutImg500).attr('data-' + (formPosition + 200), layoutImg700).attr('data-' + (formPosition + 400), layoutImg900);
    }

    var parallax = skrollr.init();
    var parallaxFlag = false;
    setTimeout(function() {
        $.stellar({
            horizontalScrolling: false
        });
    }, 500);  

    $(window).on('resize', function() {
        if (window.matchMedia("(max-width:1024px)").matches) {
            if (!parallaxFlag) {
                parallax = skrollr.init().destroy();
                $.stellar("destroy");

                parallaxFlag = true;
            }
        } else {
            if (parallaxFlag) {
                parallax = skrollr.init();
                $.stellar({
                    horizontalScrolling: false,
                    positionProperty: 'transform'
                });

                parallaxFlag = false;
            }
        }
    }).trigger('resize');

    var sectionFlag = false;

    $(window).on('resize', function() {
        if (window.matchMedia("(max-width:640px)").matches) {
            if (!sectionFlag) {
                $('.about__item-background').each(function() {
                    var src = $(this).find('img').attr('src');
                    $(this).attr('style', 'background-image: url("' + src + '")').attr('data-section-bg', '');
                });

                sectionFlag = true;
            }
        } else {
            if (sectionFlag) {
                $('.about__item-background').removeAttr('style').removeAttr('data-section-bg');

                sectionFlag = false;
            }
        }
    }).trigger('resize');

    function scrollNext(parent, arrow) {
        var parent = parent;
        var arrow = arrow;

        $(document).on('click', arrow, function(event) {
            event.preventDefault();
            var nextPosition = $(parent).next().offset().top;
            if (!$('body,html').is(':animated')) {
                $('body,html').animate({
                    'scrollTop': nextPosition
                }, 1000);
            }
        });
    }

    scrollNext('.main-banner', '.main-banner__arrow');
    scrollNext('.main-slider', '.main-slider__arrow');
    AOS.init();
    /*inView('.about__topline-img').on('enter', function(el) {
    el.classList.add('animate');
}).on('exit', function(el){
    el.classList.remove('animate');
});
inView('.news__topline-img').on('enter', function(el) {
    el.classList.add('animate');
}).on('exit', function(el){
    el.classList.remove('animate');
});*/
    inView('.about__topline-img').once('enter', function(el) {
        el.classList.add('animate');
        setTimeout(function(){
            el.classList.add('animateend')
        }, 3000);
    });
    inView('.news__topline-img').once('enter', function(el) {
        el.classList.add('animate');
        setTimeout(function(){
            el.classList.add('animateend')
        }, 3000);
    });
    $('select,input[type=checkbox]').not('no-styler').styler();
    var agreeCheckbox = $('.app-form__item--agreement #form-checkbox1');
    var agreeCheckboxChecked = agreeCheckbox.is(':checked');
    if (!agreeCheckboxChecked) {
        agreeCheckbox.closest('.app-form__block').find('.app-form__item--button button').addClass('disabled');
    } else {
        agreeCheckbox.closest('.app-form__block').find('.app-form__item--button button').removeClass('disabled');
    }
    agreeCheckbox.on('change', function(event) {
        agreeCheckboxChecked = $(this).is(':checked');
        if (!agreeCheckboxChecked) {
            agreeCheckbox.closest('.app-form__block').find('.app-form__item--button button').addClass('disabled');
        } else {
            agreeCheckbox.closest('.app-form__block').find('.app-form__item--button button').removeClass('disabled');
        }
    });
    var successPopup = $('[data-remodal-id=form]').remodal();;
    (function() {

        $.preventScrolling = function(selector, options) {

            // запрещаем прокрутку страницы при прокрутке элемента
            var defaults = {

                classes: {
                    scrolled: 'is-scrolled',
                    onTop: 'is-onTop',
                    onBottom: 'is-onBottom',
                },
                onTop: function() {},
                onBottom: function() {}
            };

            var options = $.extend({}, defaults, options);

            var scroller = $(selector);

            scroller.on('scroll', function() {

                if (scroller.scrollTop() == 0) {
                    scroller
                        .addClass(options.classes.onTop)
                        .removeClass(options.classes.onBottom);
                }

                if (scroller.scrollTop() == (scroller[0].scrollHeight - scroller.height())) {
                    scroller
                        .removeClass(options.classes.onTop)
                        .addClass(options.classes.onBottom);
                }
            });

            if (scroller[0].scrollHeight > scroller.height()) {
                scroller.addClass('with-scroll');
            } else {
                scroller.removeClass('with-scroll');
            }

            $(window).on('resize', function() {

                if (scroller[0].scrollHeight > scroller.height()) {
                    scroller.addClass('with-scroll');
                } else {
                    scroller.removeClass('with-scroll');
                }
            });

            scroller.off('mousewheel DOMMouseScroll').on('mousewheel DOMMouseScroll', function(e) {

                var scrollTo = null;

                if (e.type == 'mousewheel') {
                    scrollTo = (e.originalEvent.wheelDelta * -1);
                } else if (e.type == 'DOMMouseScroll') {
                    scrollTo = 40 * e.originalEvent.detail;
                }

                if (scrollTo && scroller[0].scrollHeight > scroller.height()) {
                    e.stopPropagation();
                    e.preventDefault();
                    $(this).scrollTop(scrollTo + $(this).scrollTop());
                }
            });
        };

    })();

    /**
     * Мобильное меню сайта
     */
    var asideMenuBtn = $('.app-header__button');
    var asideMenu = $('.b-aside-menu');
    var asideHead = $('.b-aside-menu__head');
    var asideMenuContent = $('.b-aside-menu__content');
    var asideMenuScroller = $('.b-aside-menu__scroller-content');
    var asideMenuFoot = $('.b-aside-menu__foot');


    function openAsideMenu() {
        asideMenu.addClass('js-animate js-opening');
        $('.app-header,body').addClass('menu-open');
        $('.app-header__button-icon--close').addClass('b-aside-menu__close');
    }

    function closeAsideMenu() {

        asideMenu.removeClass('js-animate');
        $('.app-header,body').removeClass('menu-open');
        $('.app-header__button-icon--close').removeClass('b-aside-menu__close');

        setTimeout(function() {
            asideMenu.removeClass('js-opening');
        }, 150);
    }

    var pxScroller = document.querySelector('.b-aside-menu__scroller');

    function pxAsideHead(event) {
        asideHead.css({
            transform: 'translateY(' + event.target.scrollTop / 1.8 + 'px)'
        });
    }

    pxScroller.addEventListener('scroll', pxAsideHead, supportsPassive ? {
        passive: true
    } : false);



    asideMenuBtn.on('pointerup', function(event) {
        event.preventDefault();
        openAsideMenu();
    });

    $(document).on('pointerup', '.b-aside-menu__close', function(event) {
        event.preventDefault();
        closeAsideMenu();
    });

    $('.b-aside-menu__overlay').on('pointerup', function(event) {
        event.preventDefault();
        closeAsideMenu();
    });

    /**
     * запрещаем прокрутку страницы при прокрутке бокового-мобильного
     */
    $.preventScrolling($('.b-aside-menu__scroller'));


    /**
     * Клонирование верхнего-левого меню в боковое-мобильное
     */
    if ($.exists('.app-header__menu')) {

        var newTopNav = $('.app-header__menu').clone();

        newTopNav.each(function(index, el) {
            $(this).find('.app-header__menu-dropdown-img').unwrap();
            $(this).find('.app-header__menu-dropdown-img').remove();
        });

        newTopNav
            .removeClass('app-header__menu')
            .addClass('aside-nav-list aside-nav-list__app-header-menu')
            .appendTo(asideMenuScroller);

    }
    /*Добавление стрелочек для li*/
    $.each(asideMenuScroller.find('li'), function(index, element) {

        if ($(element).find('ul').length) {

            var triggerIcon = ['+'].join('');

            var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');

            $(element).addClass('is-has-child');
            $(element).find(' > a').append(subMenuTrigger);
        }
    });

    $(document).on('click', '.sub-menu-trigger', function(event) {
        event.preventDefault();
        $(this).closest('li.is-has-child').find('>ul').slideToggle();
    });
    var dots = null;
    $('.main-slider__items').addClass('owl-carousel').owlCarousel({
        items: 1,
        autoplay: true,
        autoplayTimeout: 10000,
        loop: true,
        mouseDrag: false,
        touchDrag: false,
        animateOut: 'fadeOut',
        onInitialized: function() {
            dots = $('.main-slider__items .owl-dots');
            $('.main-slider__items .owl-item.active .main-slider__item-dots').empty().append(dots);
            //$('.main-slider__line').addClass('animate');
        },
        onTranslate: function() {
            //$('.main-slider__line').removeClass('animate');
        },
        onTranslated: function() {
            $('.main-slider__items .owl-item.active .main-slider__item-dots').empty().append(dots);
            //$('.main-slider__line').addClass('animate');
        },
        onResized: function() {
            $('.main-slider__items .owl-item.active .main-slider__item-dots').empty().append(dots);
        },
        onRefresh: function() {
            $('.main-slider__items .owl-item.active .main-slider__item-dots').empty().append(dots);
        }
    });
    /*$('.main-slider__item-img-inner').zoom();*/
    ;
    (function() {

        function coords(str) {
            return str.split(',');
        }


        function init(options) {
            $("#map").attr('lat');
            var ccords_map = $("#map").attr('data-cord');
            options.center = coords(ccords_map);
          /*  $.each(options.data, function(key, item) {
                item.coords = coords(item.coords);
            });*/

            google.maps.event.addDomListener(window, 'load', function() {

                var map = new google.maps.Map(document.getElementById(options.id), {
                    zoom: parseInt(options.zoom),
                    center: new google.maps.LatLng(options.center[0], options.center[1]),
                    disableDefaultUI: true
                });

                var styles1 = [{
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e9e9e9"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 29
                    }, {
                        "weight": 0.2
                    }]
                }, {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 18
                    }]
                }, {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#dedede"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                        "visibility": "on"
                    }, {
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "saturation": 36
                    }, {
                        "color": "#333333"
                    }, {
                        "lightness": 40
                    }]
                }, {
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f2f2f2"
                    }, {
                        "lightness": 19
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 17
                    }, {
                        "weight": 1.2
                    }]
                }];

                map.setOptions({
                    styles: styles1
                });

                $.each(options.data, function(key, item) {

                    var image = '/local/templates/BI_apk/images/img/map-icon.svg';

                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(options.center[0], options.center[1]),
                        map: map,
                        title: item.name,
                        icon: image
                    });

                    var infowindow = new google.maps.InfoWindow({
                        content: '<div class="baloon-content">' +
                            '<h3>' + item.name + '</h3>' +
                            item.desc +
                            '</div>'
                    });

                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.open(map, marker);
                    });

                });
            });

        }

        window.mjsMap = init;

    })();


    if ($.exists('#map')) {
        mjsMap({
            data: [{
                name: "",
                coords: "55.753753282604855,37.642133023773155",
                desc: ""
            }],
            zoom: 15,
            center: "55.753753282604855,37.642133023773155",
            id: 'map'
        });
    };
    (function($) {
        $.fn.myTabs = function(options) {

            var settings = $.extend({
                title: '.tabs__title',
                body: '.tabs__body',

                responsive: false,
                responsiveWidth: 768,

                adaptiveOffset: 0,

                activeClass: 'active',

                afterInit: $.noop(),
                afterChange: $.noop()
            }, options);

            return $(this).each(function() {
                var titles = $(this).find(settings.title);
                var bodys = $(this).find(settings.body);
                var titlesParent = titles.parent();
                var bodysParent = bodys.parent();
                var flag = false;

                titles.each(function(ind, title) {
                    $(title).attr('data-tab-index', ind);
                    bodys.eq(ind).attr('data-tab-index', ind);
                });
                if (settings.responsive) {
                    $(window).on('resize', function() {
                        if (window.matchMedia("(max-width:" + settings.responsiveWidth + "px)").matches) {
                            if (!flag) {
                                titles.filter('.' + settings.activeClass).after(bodysParent);

                                flag = true;
                            }
                        } else {
                            if (flag) {
                                titlesParent.after(bodysParent);

                                flag = false;
                            }
                        }
                    }).trigger('resize');
                }

                titles.on('click', function(event) {
                    event.preventDefault();

                    var $this = $(this);
                    var indx = $this.data('tab-index');

                    if ($this.hasClass(settings.activeClass)) {
                        return;
                    }
                    titles.removeClass(settings.activeClass);
                    $this.addClass(settings.activeClass);

                    bodys
                        .hide()
                        .removeClass(settings.activeClass)
                        .filter('[data-tab-index="' + indx + '"]')
                        .addClass(settings.activeClass)
                        .fadeIn();

                    if (settings.responsive) {
                        if (window.matchMedia("(max-width:" + settings.responsiveWidth + "px)").matches) {
                            titles.filter('.' + settings.activeClass).after(bodysParent);
                            var scrollBlock = titles.filter('.' + settings.activeClass).offset().top;
                            var fixedTop = settings.adaptiveOffset;
                            $('html, body').animate({
                                scrollTop: (scrollBlock - fixedTop)
                            }, 700);
                        }
                    }

                    if ($.isFunction(settings.afterChange)) {
                        settings.afterChange();
                    }
                });

                titles.eq(0).trigger('click');

                if ($.isFunction(settings.afterInit)) {
                    settings.afterInit();
                }
            });
        };
    })(jQuery);
    $('.new-tabs').myTabs({
        title: '.new-tabs__link',
        body: '.new-tabs__content-item'
    });
    /*$(window).on('resize', function(){
    if (window.matchMedia("(min-width:1025px)").matches){
		$('.news-item--big').each(function(index, el) {
			var newsBigOffset =  $(this).offset().left;
			$(this).css('background-position-x', newsBigOffset + 'px');
		});    	    
    }
}).trigger('resize');*/


  /*  $('.preloader__background, .preloader__content').fadeIn(0);
    $(window).load(function() {
        $('.preloader__background').delay(250).fadeOut(1500);
        $('.preloader__content').delay(250).fadeOut(750);
    });*/
    $('.scale-slider__inner').addClass('owl-carousel').owlCarousel({
        items: 3,
        center: true,
        nav: true,
        navText: ['<svg width="54" height="43" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:avocode="https://avocode.com/" viewBox="0 0 54 43"><path transform="matrix(1,0,0,1,-988,-4032)" d="M990.76035,4052.76657l46.63179,0l-17.90437,-17.49474c-0.29892,-0.2916 -0.29892,-0.76642 0,-1.05883c0.15015,-0.14674 0.34637,-0.21998 0.54253,-0.21998c0.19615,0 0.3923,0.07324 0.5426,0.21863l19.18944,18.75167c0.1528,0.13685 0.24865,0.33338 0.24865,0.55157c0,0.0005 0,0.001 0,0.00149c0,0.00049 0,0.00099 0,0.00148c0,0.19917 -0.08118,0.3897 -0.22377,0.53023l-19.21593,18.76679c-0.299,0.29214 -0.78468,0.29214 -1.08353,0c-0.29892,-0.29214 -0.29892,-0.76642 0,-1.05856l17.90958,-17.49258h-46.637c-0.42297,0 -0.76622,-0.33511 -0.76622,-0.74886c0,-0.41294 0.34325,-0.74832 0.76622,-0.74832z"/></svg>', '<svg width="54" height="43" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:avocode="https://avocode.com/" viewBox="0 0 54 43"><path transform="matrix(1,0,0,1,-988,-4032)" d="M990.76035,4052.76657l46.63179,0l-17.90437,-17.49474c-0.29892,-0.2916 -0.29892,-0.76642 0,-1.05883c0.15015,-0.14674 0.34637,-0.21998 0.54253,-0.21998c0.19615,0 0.3923,0.07324 0.5426,0.21863l19.18944,18.75167c0.1528,0.13685 0.24865,0.33338 0.24865,0.55157c0,0.0005 0,0.001 0,0.00149c0,0.00049 0,0.00099 0,0.00148c0,0.19917 -0.08118,0.3897 -0.22377,0.53023l-19.21593,18.76679c-0.299,0.29214 -0.78468,0.29214 -1.08353,0c-0.29892,-0.29214 -0.29892,-0.76642 0,-1.05856l17.90958,-17.49258h-46.637c-0.42297,0 -0.76622,-0.33511 -0.76622,-0.74886c0,-0.41294 0.34325,-0.74832 0.76622,-0.74832z"/></svg>'],
        loop: true,
        responsive: {
            0: {
                items: 1,
                center: false
            },
            721: {
                items: 3,
                center: true
            }
        }
    });
    $('.scale-slider__inner').lightGallery({
        thumbnail: false,
        autoplayControls: false,
        fullScreen: false,
        download: false,
        counter: false,
        toogleThumb: false,
        thumbMargin: 20,
        thumbWidth: 250,
        thumbContHeight: 220,
        loop: false,
        hash: false,
        zoom: false,
        actualSize: false,
        selector: '.scale-slider__item-img'
    });
    (function($) {
        $.fn.myTabs = function(options) {

            var settings = $.extend({
                title: '.tabs__title',
                body: '.tabs__body',
                images: '.tabs__images-item',

                responsive: false,
                responsiveWidth: 768,

                adaptiveOffset: 0,

                activeClass: 'active',

                afterInit: $.noop(),
                afterChange: $.noop()
            }, options);

            return $(this).each(function() {
                var titles = $(this).find(settings.title);
                var bodys = $(this).find(settings.body);
                var images = $(this).find(settings.images);
                var titlesParent = titles.parent();
                var bodysParent = bodys.parent();
                var flag = false;
                var pageHash = location.hash.substring(1);

                $(window).on('hashchange', function(event) {
                    pageHash = location.hash.substring(1);
                    titles.each(function(index, el) {
                        if ($(el).data('hash') == pageHash) {
                            $(el).trigger('click');
                        }
                    });
                });

                titles.each(function(ind, title) {
                    $(title).attr('data-tab-index', ind);
                    bodys.eq(ind).attr('data-tab-index', ind);
                    images.eq(ind).attr('data-tab-index', ind);
                });
                if (settings.responsive) {
                    $(window).on('resize', function() {
                        if (window.matchMedia("(max-width:" + settings.responsiveWidth + "px)").matches) {
                            if (!flag) {
                                titles.filter('.' + settings.activeClass).after(bodysParent);

                                flag = true;
                            }
                        } else {
                            if (flag) {
                                titlesParent.after(bodysParent);

                                flag = false;
                            }
                        }
                    }).trigger('resize');
                }

                titles.on('click', function(event) {
                    event.preventDefault();

                    var $this = $(this);
                    var indx = $this.data('tab-index');

                    if ($this.hasClass(settings.activeClass)) {
                        return;
                    }
                    titles.removeClass(settings.activeClass);
                    $this.addClass(settings.activeClass);

                    bodys
                        .hide()
                        .removeClass(settings.activeClass)
                        .filter('[data-tab-index="' + indx + '"]')
                        .addClass(settings.activeClass)
                        .fadeTo(2500, 1);

                    images
                        .hide()
                        .removeClass(settings.activeClass)
                        .filter('[data-tab-index="' + indx + '"]')
                        .addClass(settings.activeClass)
                        .fadeTo(2500, 1);

                    if (settings.responsive) {
                        if (window.matchMedia("(max-width:" + settings.responsiveWidth + "px)").matches) {
                            titles.filter('.' + settings.activeClass).after(bodysParent);
                            var scrollBlock = titles.filter('.' + settings.activeClass).offset().top;
                            var fixedTop = settings.adaptiveOffset;
                            $('html, body').animate({
                                scrollTop: (scrollBlock - fixedTop)
                            }, 700);
                        }
                    }

                    if ($.isFunction(settings.afterChange)) {
                        settings.afterChange();
                    }
                });

                if (pageHash != "") {
                    titles.each(function(index, el) {
                        if ($(el).data('hash') == pageHash) {
                            $(el).trigger('click');
                        }
                    });
                } else {
                    titles.eq(0).trigger('click');
                }

                if ($.isFunction(settings.afterInit)) {
                    settings.afterInit();
                }
            });
        };
    })(jQuery);
    /*$('.tabs').myTabs({
   /* $('.tabs').myTabs({
        title: '.tabs__link',
        body: '.tabs__content-item',
        responsive: false
    });*/
    //$('.tabs__content-item').overlayScrollbars({});
    $('.versions').on('click', function(event) {
        event.preventDefault();
        $(this).toggleClass('active');
    });
});