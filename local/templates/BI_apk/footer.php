<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>

    <footer class="app-footer">
        <div class="app-footer__inner">
            <nav class="app-footer__menu">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer_menu", Array(
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_THEME" => "site",
                    "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "COMPONENT_TEMPLATE" => ".default"
                ),
                    false
                );?>
            </nav>
            <a class="app-footer__private" href="/public-offer/">
                Политика конфиденциальности
            </a>
            <div class="app-footer__copy"> © <?=date('Y')?>. All rights reserved.</div>
        </div>
    </footer>
<div class="remodal remodal-is-initialized remodal-is-closed" data-remodal-id="form" data-remodal-options="hashTracking: false, closeOnOutsideClick: true" tabindex="-1">
    <div class="remodal-title">
        <div class="remodal-close" data-remodal-action="close"> <svg fill="#000000" height="24" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
                <path d="M0 0h24v24H0z" fill="none"/>
            </svg> </div>
    </div>
    <div class="remodal-content">
        <div class="success-block">
            <div class="success-block__title"> Спасибо! </div>
            <div class="success-block__text"> Мы свяжемся с вами в ближайшее время  </div>
        </div>
        <!-- b:success-block -->
    </div>
</div>
    <!-- /app-footer -->
    <script src="<?=SITE_TEMPLATE_PATH?>/images/scripts/plugins.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/images/scripts/main.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/additional_scripts/leng.js"></script>
    <script>
        // Фикс. исчезающих svg-иконок (xlink из спрайта) в IE, после jquery wrap
        svg4everybody();
    </script>
    <?
        CJSCore::Init(array("jquery"));
    ?>
    <script>
        $('.preloader__background, .preloader__content').fadeIn(0);
        $(window).load(function () {
            $('.preloader__background').delay(250).fadeOut(1500);
            $('.preloader__content').delay(250).fadeOut(750);
        });
    </script>
    <script src="/contacts/script.js"></script>
    <script src="/partners/script.js"></script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter48884894 = new Ya.Metrika({
                        id:48884894,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/48884894"; style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

</body>
</html>