<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$news = new bi_news();
?>

<div class="app-body">
    <main class="app-content">
        <div class="news">
            <div class="news__topline">
                <div class="news__topline-inner">
                    <div class="news__topline-layout news__topline-layout--content">
                        <div class="h1 news__topline-title" data-aos-duration="1000" data-aos="fade-up"> Новости</div>
                        <div class="news__topline-tags">
                            <?
                            $years = $news->get_years();
                            ?>
                            <?if(count($years)>1):?>
                                <div class="news__topline-tags-layout news__topline-tags-layout--content"> Архив:</div>
                                <div class="news__topline-tags-layout news__topline-tags-layout--links">
                                    <?foreach ($years as $key => $year):?>
                                        <a class="<?if($key==0):?>active<?endif;?> news__topline-tags-link" href="javascript:void(0)"><?=$year?></a>
                                    <?endforeach;?>
                                </div>
                            <?endif;?>
                        </div>
                    </div>
                    <div class="news__topline-layout news__topline-layout--img">
                        <div class="news__topline-img">
                            <div class="news__topline-img-parallax">
                                <?$APPLICATION->IncludeFile(
                                    '/local/templates/BI_apk/include_areas/news/image_block.php',
                                    Array(),
                                    Array("MODE"=>"html")
                                );?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="news__content">
                <div class="news__content-inner">
                    <?
                    $news_count = $news->total_count();
                    /*  $pages = ceil($news_count/4);*/
                    $pages = 1;
                    $position_right = false;
                    for ($i = 1; $i <= $pages; $i++): ?>
                        <?
                        $big_exist = null;
                        $news_list = $news->get_news(array('PROPERTY_PROP_BIG_VALUE'=>''),$i);
                        if(!empty($news_list)) {
                            foreach ($news_list as $key => $list_item) {
                                if ($list_item['PROPS']['PROP_BIG']['VALUE'] == 'Да') {
                                    $big_exist = $key;
                                }
                            }
                        }
                        ?>
                        <div class="news__content-item <?if($position_right):?> news__content-item--right<?endif;?>">
                            <?if($big_exist==0||$big_exist!=null):?>

                                <div class="news__content-item-layout news__content-item-layout--big">
                                    <article class="news-item news-item--big">
                                        <div class="news-item__background"><img
                                                    src="<?=$news_list[$big_exist]['PREVIEW_PICTURE']?>"
                                                    alt="<?=$news_list[$big_exist]['NAME']?>"
                                                    title="<?=$news_list[$big_exist]['NAME']?>"
                                                    data-stellar-ratio="0.5"
                                            >
                                        </div>
                                        <div class="news-item__inner">
                                            <h3 class="news-item__title"><span><?=$news_list[$big_exist]['NAME']?></span>
                                            </h3>
                                            <?
                                            $date = explode(" ", $news_list[$big_exist]['DATE_ACTIVE_FROM']);
                                            ?>
                                            <div class="news-item__date"> <?=$date[0]?></div>
                                            <div class="news-item__text"> <?=$news_list[$big_exist]['PREVIEW_TEXT']?>
                                            </div>
                                            <div class="news-item__link"><a href="/news/<?=$news_list[$big_exist]['CODE']?>/">
                                                    Читать полностью
                                                </a></div>
                                        </div>
                                    </article>
                                </div>
                            <?endif;?>
                            <?if(!empty($news_list)):?>

                                <div class="news__content-item-layout news__content-item-layout--items">
                                    <?foreach ($news_list as $news_element):?>
                                        <?
                                        if ($news_element['PROPS']['PROP_BIG']['VALUE'] == 'Да') {
                                            continue;
                                        }
                                        ?>
                                        <article class="news-item">
                                            <div class="news-item__square">
                                                <div class="news-item__square-item news-item__square-item--first"></div>
                                                <div class="news-item__square-item news-item__square-item--second"></div>
                                                <div class="news-item__square-item news-item__square-item--third"></div>
                                                <div class="news-item__square-item news-item__square-item--fourth"></div>
                                            </div>
                                            <h3 class="news-item__title"> <?=$news_element['NAME']?> </h3>
                                            <?
                                            $date = explode(" ", $news_element['DATE_ACTIVE_FROM']);
                                            ?>
                                            <div class="news-item__date"> <?=$date[0]?></div>
                                            <div class="news-item__text">
                                                <?=$news_element['PREVIEW_TEXT']?>
                                            </div>
                                            <div class="news-item__link"><a href="/news/<?=$news_element['CODE']?>/">
                                                    Читать полностью
                                                </a></div>
                                        </article>
                                    <?endforeach;?>
                                </div>
                            <?endif;?>
                        </div>
                        <? $position_right = true; ?>
                    <?endfor;?>
                </div>
            </div>
            <?if($news_count>4):?>
                <div class="news__more" data-total-count="<?=$news_count?>" data-current-page="1">
                    <div class="news__more-link">
                        <button class="more-btn">
                            Загрузить больше
                        </button>
                    </div>
                </div>
            <?endif;?>
        </div>
        <!-- b:news -->
    </main>
</div>