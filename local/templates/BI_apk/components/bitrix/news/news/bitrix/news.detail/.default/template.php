<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$news = new bi_news();

$SEO = new mySEO();
if($arResult['PROPERTIES']['TEXT_BLOCK1']['~VALUE']['TEXT']!=""){
    $desc = $SEO::GetDescriptionFromText($arResult['PROPERTIES']['TEXT_BLOCK1']['~VALUE']['TEXT']);
}
else{
    $desc = $SEO::GetDescriptionFromText($arResult['PROPERTIES']['TEXT_BLOCK2']['~VALUE']['TEXT']);
}
if($APPLICATION->GetDirProperty("description")=="АПК Астраханский"||$APPLICATION->GetDirProperty("description")=="") {
    $APPLICATION->SetPageProperty("ogdescription", $desc);
    $APPLICATION->SetPageProperty("description", $desc);
}
$APPLICATION->SetPageProperty("ogImage", $arResult["DETAIL_PICTURE"]["SRC"]);
$APPLICATION->SetPageProperty('title', $arResult['NAME']." | ООО «АПК Астраханский»");
?>
<div class="app-body">
    <main class="app-content">
        <div class="about about--news-article">
            <div class="about__inner">
                <div class="about__topline">
                    <div class="about__topline-layout about__topline-layout--content">
                        <div class="h1 about__topline-title" data-aos-duration="1000" data-aos="fade-up"><?=$arResult['NAME']?>
                        </div>
                        <?
                            $date = explode(' ', $arResult['ACTIVE_FROM']);
                        ?>
                        <div class="about__topline-date" data-aos-duration="1000" data-aos="fade-up"> <?=$date[0]?></div>
                        <a class="about__topline-all-link" href="/news/">
                            <span class="svg-icon svg-icon--slider-arrow"><svg class="svg-icon__link"><use
                                            xlink:href="#slider-arrow"/></svg></span>
                            Все новости
                        </a></div>
                    <div class="about__topline-layout about__topline-layout--img">
                        <div class="about__topline-img">
                            <div class="about__topline-img-parallax"><img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['NAME']?>"
                                                                          data-stellar-ratio="0.5"></div>
                        </div>
                    </div>
                </div>
                <?if($arResult['PROPERTIES']['TEXT_BLOCK1']['~VALUE']['TEXT']!=""):?>
                    <div class="about__content" data-aos-duration="1000" data-aos="fade-up">
                        <?=$arResult['PROPERTIES']['TEXT_BLOCK1']['~VALUE']['TEXT']?>
                    </div>
                <?endif;?>
                <?if(!empty($arResult['PROPERTIES']['Micro']['VALUE'])):?>
                    <div class="about__items">
                        <?foreach ($arResult['PROPERTIES']['Micro']['VALUE'] as $micro_news_id):?>
                            <? $micro_news_list = $news->get_micro_news_by_id($micro_news_id); ?>
                            <?foreach ($micro_news_list as $micro_news):?>
                                <div class="about__item">
                                    <div class="about__item-background"><img src="<?=$micro_news['PREVIEW_PICTURE']?>" alt="<?=$micro_news['NAME']?>"></div>
                                    <div class="about__item-content">
                                        <div class="about__item-title"><span><?=$micro_news['NAME']?></span></div>
                                        <div class="about__item-text"> <?=$micro_news['PREVIEW_TEXT']?></div>
                                    </div>
                                </div>
                            <?endforeach;?>
                        <?endforeach;?>
                    </div>
                <?endif;?>
                <?if($arResult['PROPERTIES']['TEXT_BLOCK2']['~VALUE']['TEXT']!=""):?>
                    <div class="about__content" data-aos-duration="1000" data-aos="fade-up">
                        <?=$arResult['PROPERTIES']['TEXT_BLOCK2']['~VALUE']['TEXT']?>
                    </div>
                <?endif;?>

                <?if($arResult['PROPERTIES']['IMG_BLOCK']['VALUE']!=""):?>
                    <div class="about__banner">
                        <div class="about__banner-inner"><img src="<?=CFile::GetPath($arResult['PROPERTIES']['IMG_BLOCK']['VALUE'])?>" alt="<?=$arResult['NAME']?>"
                                                              data-stellar-ratio="0.5"></div>
                        <div class="about__banner-square" data-aos="zoom-in" data-aos-duration="1000"></div>
                    </div>
                <?endif;?>

                <?if($arResult['PROPERTIES']['TEXT_BLOCK3']['~VALUE']['TEXT']!=""):?>
                    <div class="about__content" data-aos-duration="1000" data-aos="fade-up">
                        <?=$arResult['PROPERTIES']['TEXT_BLOCK3']['~VALUE']['TEXT']?>
                    </div>
                <?endif;?>
                <?if($arResult['PROPERTIES']['GALLERY']['VALUE']!=""):?>
                <div class="about__slider">
                    <div class="scale-slider scale-slider--about">
                        <div class="scale-slider__inner">
                            <?
                                $code ="";
                                $res = CIBlockSection::GetByID($arResult['PROPERTIES']['GALLERY']['VALUE']);
                                if($ar_res = $res->GetNext()){
                                    $code = $ar_res['CODE'];
                                }
                                $media = new bi_data($code);
                            ?>
                            <?foreach ($media->get_pictures() as $slider_item):?>
                                <div class="scale-slider__item">
                                    <a class="scale-slider__item-img" href="<?=$slider_item['DETAIL_PICTURE']?>">
                                        <img
                                            src="<?=$slider_item['PREVIEW_PICTURE']?>"
                                            alt="<?=$slider_item['NAME']?>"
                                            title="<?=$slider_item['NAME']?>"
                                        >
                                    </a>
                                    <div class="scale-slider__item-content"> <?=$slider_item['PREVIEW_TEXT']?></div>
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
                    <!-- b:scale-slider -->
                </div>
                <?endif;?>



                <div class="about__news-line">
                    <?
                    if (CModule::IncludeModule("iblock")) :
                    $page = array();
                    $rs=CIBlockElement::GetList(array("active_from" => "desc"), array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"]), false, array("nElementID"=>$arResult['ID'], "nPageSize"=>1), array("ID", "NAME", "DETAIL_PAGE_URL"));
                    while($ar=$rs->GetNext()){
                        $page[] = $ar;
                    }
                    ?>
                    <div class="about__news-line-links">
                        <?if (count($page) == 2 && $arResult['ID'] == $page[0]['ID']):?>
                            <a class="about__news-line-link about__news-line-link--prev" href="/news/<?=$page[1]["CODE"]?>/">
                            <span class="svg-icon svg-icon--slider-arrow">
                                <svg class="svg-icon__link"><use xlink:href="#slider-arrow"/></svg>
                            </span>
                                Предыдущая новость
                            </a>
                            <a class="about__news-line-link about__news-line-link--all" href="/news/">Все новости</a>
                        <?elseif (count($page) == 3):?>
                            <a class="about__news-line-link about__news-line-link--prev" href="/news/<?=$page[0]["CODE"]?>/">
                            <span class="svg-icon svg-icon--slider-arrow">
                                <svg class="svg-icon__link"><use xlink:href="#slider-arrow"/></svg>
                            </span>
                                Предыдущая новость
                            </a>
                            <a class="about__news-line-link about__news-line-link--all" href="/news/">Все новости</a>
                            <a class="about__news-line-link about__news-line-link--next" href="/news/<?=$page[2]["CODE"]?>/">
                                Следующая новость
                                <span class="svg-icon svg-icon--slider-arrow">
                                <svg class="svg-icon__link"><use xlink:href="#slider-arrow"/></svg>
                            </span>
                            </a>
                        <?elseif (count($page) == 2 && $arResult['ID'] == $page[1]['ID']):?>
                            <a class="about__news-line-link about__news-line-link--all" href="/news/">Все новости</a>
                            <a class="about__news-line-link about__news-line-link--next" href="/news/<?=$page[0]["CODE"]?>/">
                                Следующая новость
                                <span class="svg-icon svg-icon--slider-arrow">
                                <svg class="svg-icon__link"><use xlink:href="#slider-arrow"/></svg>
                            </span>
                            </a>
                        <?endif;?>
                    <?endif;?>
                    </div>
                </div>
            </div>
        </div>
        <!-- b:about -->
    </main>
</div>