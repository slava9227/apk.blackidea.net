<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
$dataObj = new bi_data();
$dataAr = $dataObj->getIBlockData(6);
$arSectionData = $dataObj->getIBlockData(6, ['PROPERTY_SECTION_INFO_VALUE' => 'Да']);
?>

<div class="tabs__links">
 <? foreach ($dataAr as $item) { ?>
     <?
         if($item['PROPS']['SECTION_INFO']['VALUE'] === 'Да'){
            continue;
         }
     ?>
     <a href="<?='http://' . $_SERVER['HTTP_HOST'] . '/production/' . $item['CODE']?>/" class="tabs__link" style="text-decoration: none">
         <div class="tabs__link-icon">
            <span class="svg-icon svg-icon--tabs1">
                <?=$item['PROPS']['SVG']['~VALUE']['TEXT']?>
            </span>
         </div>
         <div class="tabs__link-title"> <?=$item['NAME']?></div>
     </a>
 <? } ?>
</div>
<?foreach ($arSectionData as $arResult):?>
<?foreach ($arResult['PROPS']['TEXTS']['~VALUE'] as $key => $item):?>
    <?if($key%2 == 0):?>
        <div class="tabs__bottom tabs__bottom--right">
            <div class="tabs__images">
                <div class="tabs__images-item">
                    <div class="tabs__images-inner">
                        <div class="tabs__images-block" data-stellar-ratio="0.5">
                            <img
                                alt="<?=$arResult['PROPS']['TEXT_TITLE']['VALUE'][$key]?>"
                                title="<?=$arResult['PROPS']['TEXT_TITLE']['VALUE'][$key]?>"
                                src="<?=CFile::GetPath($arResult['PROPS']['IMGS']['VALUE'][$key])?>">
                            <div class="tabs__images-content"> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs__content">
                <div class="tabs__content-item">
                    <div class="tabs__content-item-inner">
                        <div class="tabs__content-item-title"><span><?=$arResult['PROPS']['TEXT_TITLE']['VALUE'][$key]?></span>
                        </div>
                        <div class="tabs__content-item-text">
                            <?=$item['TEXT']?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?else:?>
        <div class="tabs__bottom tabs__bottom--left">
            <div class="tabs__images">
                <div class="tabs__images-item">
                    <div class="tabs__images-inner">
                        <div class="tabs__images-block" data-stellar-ratio="0.5">
                            <img
                                alt="<?=$arResult['PROPS']['TEXT_TITLE']['VALUE'][$key]?>"
                                title="<?=$arResult['PROPS']['TEXT_TITLE']['VALUE'][$key]?>"
                                src="<?=CFile::GetPath($arResult['PROPS']['IMGS']['VALUE'][$key])?>">
                            <div class="tabs__images-content"> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs__content">
                <div class="tabs__content-item">
                    <div class="tabs__content-item-inner">
                        <div class="tabs__content-item-title"><span><?=$arResult['PROPS']['TEXT_TITLE']['VALUE'][$key]?></span>
                        </div>
                        <div class="tabs__content-item-text">
                            <?=$item['TEXT']?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?endif;?>
<?endforeach;?>
<?endforeach;?>



