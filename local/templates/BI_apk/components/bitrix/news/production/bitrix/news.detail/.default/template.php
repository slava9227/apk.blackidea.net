<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$dataObj = new bi_data();
$dataAr = $dataObj->getIBlockData(6);
$SEO = new mySEO();
$desc = $SEO::GetDescriptionFromText($arResult['PROPERTIES']['TEXTS']['~VALUE'][0]['TEXT']);
if($APPLICATION->GetDirProperty("description")=="АПК Астраханский"||$APPLICATION->GetDirProperty("description")=="") {
    $APPLICATION->SetPageProperty("ogdescription", $desc);
    $APPLICATION->SetPageProperty("description", $desc);
}
$APPLICATION->SetPageProperty("ogImage", $arResult["DETAIL_PICTURE"]["SRC"]);
$APPLICATION->SetPageProperty('title', $arResult['NAME']." | ООО «АПК Астраханский»");
//p($arResult);
?>
<div class="app-body">
    <main class="app-content">
        <div class="production">
            <div class="production__inner">
                <div class="h1 production__title"> <?=$arResult['NAME']?></div>
                <div class="production__text">
                    <?=$arResult['~PREVIEW_TEXT']?>
                </div>
            </div>
            <?
                $bgImagePath = CFile::GetPath($arResult['PROPERTIES']['BG_IMAGE']['~VALUE']);
            ?>
            <div class="production__background" data-section-bg="<?=$bgImagePath?>" style="z-index: -1">
                <img src="<?=$bgImagePath?>" alt="<?=$arResult['PROPERTIES']['BG_IMAGE']['~NAME']?>">
            </div>
        </div>
        <!-- b:production -->
        <div class="tabs">
            <div class="tabs__inner">
                <div class="tabs__links">
                    <? foreach ($dataAr as $item) { ?>
                        <?
                            if($item['PROPS']['SECTION_INFO']['VALUE'] === 'Да'){
                                continue;
                            }
                        ?>
                        <a href="<?='http://' . $_SERVER['HTTP_HOST'] . '/production/' . $item['CODE']?>/" class="tabs__link" style="text-decoration: none">
                            <div class="tabs__link-icon">
                                <span class="svg-icon svg-icon--tabs1">
                                    <?=$item['PROPS']['SVG']['~VALUE']['TEXT']?>
                                </span>
                            </div>
                            <div class="tabs__link-title"> <?=$item['NAME']?></div>
                        </a>
                    <? } ?>
                </div>
                <?foreach ($arResult['PROPERTIES']['TEXTS']['~VALUE'] as $key => $item):?>
                    <?if($key%2 == 0):?>
                        <div class="tabs__bottom tabs__bottom--right">
                            <div class="tabs__images">
                                <div class="tabs__images-item">
                                    <div class="tabs__images-inner">
                                        <div class="tabs__images-block" data-stellar-ratio="0.5">
                                            <img
                                                alt="<?=$arResult['PROPERTIES']['TEXT_TITLE']['VALUE'][$key]?>"
                                                title="<?=$arResult['PROPERTIES']['TEXT_TITLE']['VALUE'][$key]?>"
                                                src="<?=CFile::GetPath($arResult['PROPERTIES']['IMGS']['VALUE'][$key])?>">
                                            <div class="tabs__images-content"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__content">
                                <div class="tabs__content-item">
                                    <div class="tabs__content-item-inner">
                                        <div class="tabs__content-item-title"><span><?=$arResult['PROPERTIES']['TEXT_TITLE']['VALUE'][$key]?></span>
                                        </div>
                                        <div class="tabs__content-item-text">
                                            <?=$item['TEXT']?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?else:?>
                        <div class="tabs__bottom tabs__bottom--left">
                            <div class="tabs__images">
                                <div class="tabs__images-item">
                                    <div class="tabs__images-inner">
                                        <div class="tabs__images-block" data-stellar-ratio="0.5">
                                            <img
                                                alt="<?=$arResult['PROPERTIES']['TEXT_TITLE']['VALUE'][$key]?>"
                                                title="<?=$arResult['PROPERTIES']['TEXT_TITLE']['VALUE'][$key]?>"
                                                src="<?=CFile::GetPath($arResult['PROPERTIES']['IMGS']['VALUE'][$key])?>">
                                            <div class="tabs__images-content"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__content">
                                <div class="tabs__content-item">
                                    <div class="tabs__content-item-inner">
                                        <div class="tabs__content-item-title"><span><?=$arResult['PROPERTIES']['TEXT_TITLE']['VALUE'][$key]?></span>
                                        </div>
                                        <div class="tabs__content-item-text">
                                            <?=$item['TEXT']?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?endif;?>
                <?endforeach;?>
            </div>
        </div>
        <!-- b:tabs -->
    </main>
</div>
<div class="app-body__fake-footer"></div>