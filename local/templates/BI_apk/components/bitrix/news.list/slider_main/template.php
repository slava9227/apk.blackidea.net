<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<?foreach($arResult["ITEMS"] as $arItem):?>
    <div class="main-slider__item" style="background-image: url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>')">
        <div class="main-slider__item-inner">
            <div class="main-slider__item-content" data-aos="fade-up" data-aos-duration="2000">
                <div class="main-slider__item-title"> <?=$arItem['NAME']?></div>
                <div class="main-slider__item-text"> <?=$arItem['DETAIL_TEXT']?>
                </div>
                <div class="main-slider__item-dots"></div>
            </div>
            <div class="main-slider__item-img">
                <div class="main-slider__item-img-inner"><img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="">
                </div>
                <div class="main-slider__item-img-mask"></div>
                <div class="main-slider__square">
                    <div class="main-slider__square-item main-slider__square-item--first"></div>
                    <div class="main-slider__square-item main-slider__square-item--second"></div>
                    <div class="main-slider__square-item main-slider__square-item--third"></div>
                    <div class="main-slider__square-item main-slider__square-item--fourth"></div>
                </div>
            </div>
        </div>
    </div>
<?endforeach;?>
