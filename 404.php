<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");
?>

    <div class="app-body">
        <main class="app-content">
            <div class="page-404">
                <div class="page-404__inner">
                    <div class="page-404__topline"> Вы ищете страницу,<br>которой не существует.</div>
                    <div class="page-404__img"><img src="<?=SITE_TEMPLATE_PATH?>/images/files/404.png" alt=""></div>
                    <div class="page-404__bottom"> Пожалуйста, перейдите на <br><a href="/">главную страницу
                            сайта</a></div>
                </div>
            </div>
            <!-- b:404 -->
        </main>
    </div>
    <div class="app-body__fake-footer"></div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>