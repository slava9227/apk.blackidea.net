<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$arEventFields = [
    'NAME' => trim($_POST['name']),
    'EMAIL' => trim($_POST['email']),
    'THEME' => trim($_POST['theme']),
    'MESSAGE' => trim($_POST['message']),

];
CEvent::Send('FEEDBACK_FORM', 's1', $arEventFields);
echo 'ok';

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>