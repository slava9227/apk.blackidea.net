<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
$GLOBALS['APPLICATION']->RestartBuffer();
$news = new bi_news();
if($_REQUEST['TYPE']=='year'):
$news_count = $news->total_count(array('SECTION_CODE'=>$_REQUEST['SECTION_CODE']));
$pages = 1;
$position_right = false;
for ($i = 1; $i <= $pages; $i++): ?>
    <?
    $big_exist = null;
    $news_list = $news->get_news(array('PROPERTY_PROP_BIG_VALUE'=>'', 'SECTION_CODE'=>$_REQUEST['SECTION_CODE']),$i);
    if(!empty($news_list)) {
        foreach ($news_list as $key => $list_item) {
            if ($list_item['PROPS']['PROP_BIG']['VALUE'] == 'Да') {
                $big_exist = $key;
            }
        }
    }
    ?>
    <div class="news__content-item <?if($position_right):?> news__content-item--right<?endif;?>">
        <input type="text" class="count-of-year" value="<?=$news_count?>">
        <?if($big_exist==0||$big_exist!=null):?>

            <div class="news__content-item-layout news__content-item-layout--big">
                <article class="news-item news-item--big">
                    <div class="news-item__background"><img src="<?=$news_list[$big_exist]['PREVIEW_PICTURE']?>" alt="<?=$news_list[$big_exist]['NAME']?>"
                                                            data-stellar-ratio="0.5"></div>
                    <div class="news-item__inner">
                        <h3 class="news-item__title"><span><?=$news_list[$big_exist]['NAME']?></span>
                        </h3>
                        <?
                        $date = explode(" ", $news_list[$big_exist]['DATE_ACTIVE_FROM']);
                        ?>
                        <div class="news-item__date"> <?=$date[0]?></div>
                        <div class="news-item__text"> <?=$news_list[$big_exist]['PREVIEW_TEXT']?>
                        </div>
                        <div class="news-item__link"><a href="/news/<?=$news_list[$big_exist]['CODE']?>/">
                                Читать полностью
                            </a></div>
                    </div>
                </article>
            </div>
        <?endif;?>
        <?if(!empty($news_list)):?>

            <div class="news__content-item-layout news__content-item-layout--items">
                <?foreach ($news_list as $news_element):?>
                    <?
                    if ($news_element['PROPS']['PROP_BIG']['VALUE'] == 'Да') {
                        continue;
                    }
                    ?>
                    <article class="news-item">
                        <div class="news-item__square">
                            <div class="news-item__square-item news-item__square-item--first"></div>
                            <div class="news-item__square-item news-item__square-item--second"></div>
                            <div class="news-item__square-item news-item__square-item--third"></div>
                            <div class="news-item__square-item news-item__square-item--fourth"></div>
                        </div>
                        <h3 class="news-item__title"> <?=$news_element['NAME']?> </h3>
                        <?
                            $date = explode(" ", $news_element['DATE_ACTIVE_FROM']);
                        ?>
                        <div class="news-item__date"> <?=$date[0]?></div>
                        <div class="news-item__text">
                            <?=$news_element['PREVIEW_TEXT']?>
                        </div>
                        <div class="news-item__link"><a href="/news/<?=$news_element['CODE']?>/">
                                Читать полностью
                            </a></div>
                    </article>
                <?endforeach;?>
            </div>
        <?endif;?>
    </div>
    <? $position_right = true; ?>
<?endfor;?>
<?elseif ($_REQUEST['TYPE'] == 'more'):?>
<?
    $pages = $_REQUEST['PAGE'];
    $pages % 2 == 0 ? $position_right = true : $position_right = false;
    $big_exist = null;
    $news_list = $news->get_news(array('SECTION_CODE'=>$_REQUEST['SECTION_CODE']),$pages);
    if(!empty($news_list)) {
        foreach ($news_list as $key => $list_item) {
            if ($list_item['PROPS']['PROP_BIG']['VALUE'] == 'Да') {
                $big_exist = $key;
            }
        }
    }
    ?>
    <div class="news__content-item <?if($position_right):?> news__content-item--right<?endif;?>">
        <input type="text">
        <?if($big_exist==0||$big_exist!=null):?>

            <div class="news__content-item-layout news__content-item-layout--big">
                <article class="news-item news-item--big">
                    <div class="news-item__background"><img src="<?=$news_list[$big_exist]['PREVIEW_PICTURE']?>" alt="<?=$news_list[$big_exist]['NAME']?>"
                                                            data-stellar-ratio="0.5"></div>
                    <div class="news-item__inner">
                        <h3 class="news-item__title"><span><?=$news_list[$big_exist]['NAME']?></span>
                        </h3>
                        <?
                        $date = explode(" ", $news_list[$big_exist]['DATE_ACTIVE_FROM']);
                        ?>
                        <div class="news-item__date"> <?=$date[0]?></div>
                        <div class="news-item__text"> <?=$news_list[$big_exist]['PREVIEW_TEXT']?>
                        </div>
                        <div class="news-item__link"><a href="/news/<?=$news_list[$big_exist]['CODE']?>/">
                                Читать полностью
                            </a></div>
                    </div>
                </article>
            </div>
        <?endif;?>
        <?if(!empty($news_list)):?>

            <div class="news__content-item-layout news__content-item-layout--items">
                <?foreach ($news_list as $news_element):?>
                    <?
                    if ($news_element['PROPS']['PROP_BIG']['VALUE'] == 'Да') {
                        continue;
                    }
                    ?>
                    <article class="news-item">
                        <div class="news-item__square">
                            <div class="news-item__square-item news-item__square-item--first"></div>
                            <div class="news-item__square-item news-item__square-item--second"></div>
                            <div class="news-item__square-item news-item__square-item--third"></div>
                            <div class="news-item__square-item news-item__square-item--fourth"></div>
                        </div>
                        <h3 class="news-item__title"> <?=$news_element['NAME']?> </h3>
                        <?
                        $date = explode(" ", $news_element['DATE_ACTIVE_FROM']);
                        ?>
                        <div class="news-item__date"> <?=$date[0]?></div>
                        <div class="news-item__text">
                            <?=$news_element['PREVIEW_TEXT']?>
                        </div>
                        <div class="news-item__link"><a href="/news/<?=$news_element['CODE']?>/">
                                Читать полностью
                            </a></div>
                    </article>
                <?endforeach;?>
            </div>
        <?endif;?>
    </div>
    <? $position_right = true; ?>
<?endif;?>

