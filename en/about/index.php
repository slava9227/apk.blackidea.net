<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('О продукте');
$dataObj = new bi_data();

$videoBoxesArr = $dataObj->getIBlockData(12);

$titlePicture = $dataObj->get_pictures('about');
if(isset($titlePicture[0])){
    $titlePicture = $titlePicture[0]['PREVIEW_PICTURE'];
}else{
    $titlePicture = '';
}
?>
<div class="app-body">
    <main class="app-content">
        <div class="about about--page">
            <div class="about__inner">
                <div class="about__topline">
                    <div class="about__topline-layout about__topline-layout--content">
                        <div class="h1 about__topline-title" data-aos-duration="1000" data-aos="fade-up"> О продукте
                        </div>
                    </div>
                    <div class="about__topline-layout about__topline-layout--img">
                        <div class="about__topline-img">
                            <div class="about__topline-img-parallax">
                                <img src="<?=$titlePicture?>" alt="" data-stellar-ratio="0.5"></div>
                        </div>
                    </div>
                </div>
                <div class="about__content" data-aos-duration="1000" data-aos="fade-up">
                    <div class="about__content-topline">
                        <span>
							<?$APPLICATION->IncludeFile(
                                '/local/templates/BI_apk/include_areas/about/first_article_header.php',
                                Array(),
                                Array("MODE"=>"html")
                            );?>
						</span>
                    </div>
                    <div class="about__content-bottom">
                        <?$APPLICATION->IncludeFile(
                            '/local/templates/BI_apk/include_areas/about/first_article.php',
                            Array(),
                            Array("MODE"=>"html")
                        );?>
                    </div>
                </div>
                <div class="about__items">
                    <?foreach ($videoBoxesArr as $item) {?>
                        <div class="about__item <?if($item['PROPS']['BLUE_BG']['VALUE']){?>about__item--blue<?}?>">
                            <div class="about__item-background">
                                <img src="<?=$item['PREVIEW_PICTURE']?>" alt="<?=$item['NAME']?>">
                            </div>
                            <div class="about__item-content">
                                <div class="about__item-title"><span><?=$item['NAME']?></span></div>
                                <div class="about__item-text">
                                    <?=$item['PREVIEW_TEXT']?>
                                </div>
                                <a class="about__item-link" href="<?=$item['PROPS']['VIDEO_LINK']['VALUE']?>">
                                    <span class="about__item-link-icon">
                                        <span class="svg-icon svg-icon--play"><svg class="svg-icon__link"><use
                                                        xlink:href="#play"/></svg></span>
                                    </span>
                                    <span class="about__item-link-text">
                                        Смотреть видео
                                    </span>
                                </a>
                            </div>
                        </div>
                    <?}?>
                </div>
                <div class="about__content" data-aos-duration="1000" data-aos="fade-up">
                    <div class="about__content-topline">
                        <span>
							<?$APPLICATION->IncludeFile(
                                '/local/templates/BI_apk/include_areas/about/second_article_header.php',
                                Array(),
                                Array("MODE"=>"html")
                            );?>
						</span>
                    </div>
                    <div class="about__content-bottom">
                        <?$APPLICATION->IncludeFile(
                            '/local/templates/BI_apk/include_areas/about/second_article.php',
                            Array(),
                            Array("MODE"=>"html")
                        );?>
                    </div>
                </div>
                <div class="about__slider">
                    <div class="scale-slider scale-slider--about">
                        <div class="scale-slider__inner">
                            <?foreach ($dataObj->get_pictures('about', 2 , true) as $slider_item){?>
                                <div class="scale-slider__item">
                                    <a class="scale-slider__item-img" href="<?=$slider_item['DETAIL_PICTURE']?>">
                                        <img src="<?=$slider_item['PREVIEW_PICTURE']?>" alt="<?=$slider_item['NAME']?>">
                                    </a>
                                    <div class="scale-slider__item-content"> <?=$slider_item['PREVIEW_TEXT']?></div>
                                </div>
                            <?}?>
                        </div>
                    </div>
                    <!-- b:scale-slider -->
                </div>
            </div>
        </div>
        <!-- b:about -->
    </main>
</div>
<div class="app-body__fake-footer"></div>
<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>
