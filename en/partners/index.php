<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Новости');
$bi_data = new bi_data();

$pathToTitlePicture = '';
$pictureAfterBrands = '';
$pictureNearForm = '';

$partnersBigPictures = $bi_data->get_pictures('partners_big_pictures');
if(isset($partnersBigPictures[0])){
    $pathToTitlePicture = $partnersBigPictures[0]['PREVIEW_PICTURE'];
}
if(isset($partnersBigPictures[1])){
    $pictureAfterBrands = $partnersBigPictures[1]['PREVIEW_PICTURE'];
}
if(isset($partnersBigPictures[2])){
    $pictureNearForm = $partnersBigPictures[2]['PREVIEW_PICTURE'];
}

$partnersBrands = $bi_data->get_pictures('partners_brands_pictures');

?>
<div class="app-body">
    <main class="app-content">
        <div class="about about--partners">
            <div class="about__inner">
                <div class="about__topline">
                    <div class="about__topline-layout about__topline-layout--content">
                        <div class="h1 about__topline-title" data-aos-duration="1000" data-aos="fade-up"> Партнеры</div>
                    </div>
                    <div class="about__topline-layout about__topline-layout--img">
                        <div class="about__topline-img">
                            <div class="about__topline-img-parallax">
                                <img src="<?=$pathToTitlePicture?>" alt="" data-stellar-ratio="0.5">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about__content" data-aos-duration="1000" data-aos="fade-up">
                    <div class="about__content-topline">
                        <span>
                            <?$APPLICATION->IncludeFile(
                                '/local/templates/BI_apk/include_areas/partners/start_text_header.php',
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                        </span>
                    </div>
                    <div class="about__content-bottom">
                        <?$APPLICATION->IncludeFile(
                            '/local/templates/BI_apk/include_areas/partners/start_text.php',
                            Array(),
                            Array("MODE"=>"html")
                        );?>
                    </div>
                </div>
                <div class="about__links">
                    <?foreach ($partnersBrands as $item){?>
                        <div class="about__link-elem">
                            <div class="about__link-item"><img src="<?=$item['PREVIEW_PICTURE']?>" alt="<?=$item['NAME']?>"></div>
                        </div>
                    <?}?>
                </div>
            </div>
        </div>
        <!-- b:about -->
        <div class="partners">
            <div class="partners__inner">
                <div class="partners__layout partners__layout--img">
                    <div class="partners__img"><img src="<?=$pictureAfterBrands?>" alt="" data-stellar-ratio="0.5">
                    </div>
                </div>
                <div class="partners__layout partners__layout--content">
                    <div class="partners__content">
                        <div class="partners__title">
                            <span>
                            <?$APPLICATION->IncludeFile(
                                '/local/templates/BI_apk/include_areas/partners/title_after_brands.php',
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                            </span>
                        </div>
                        <div class="partners__text">
                            <?$APPLICATION->IncludeFile(
                                '/local/templates/BI_apk/include_areas/partners/text_after_brands.php',
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- b:partners -->
        <form class="app-form app-form--partners">
            <div class="app-form__layout app-form__layout--form">
                <div class="app-form__block">
                    <div class="app-form__title"> Напишите нам</div>
                    <div class="app-form__item app-form__item--input">
                        <div class="app-form__item-control"><input type="text" placeholder="Ваше имя" name="name"></div>
                    </div>
                    <div class="app-form__item app-form__item--select">
                        <div class="app-form__item-control"><select name="theme">
                                <option selected value="Без темы">Тема письма</option>
                                <option value="Жалоба">
                                    Жалоба
                                </option>
                                <option value="Предложение">
                                    Предложение
                                </option>
                            </select></div>
                    </div>
                    <div class="app-form__item app-form__item--input">
                        <div class="app-form__item-control"><input type="email" required placeholder="E-mail" name="email"></div>
                    </div>
                    <div class="app-form__item app-form__item--textarea">
                        <div class="app-form__item-control"><textarea placeholder="Ваше сообщение" name="message"></textarea></div>
                    </div>
                    <div class="app-form__item app-form__item--agreement">
                        <div class="app-form__item-control"><input checked type="checkbox" required id="form-checkbox1">
                        </div>
                        <div class="app-form__item-label"><label for="form-checkbox1">Я соглашаюсь на обработку <a
                                        href="/public-offer/">персональных данных</a></label></div>
                    </div>
                    <div class="app-form__item app-form__item--button">
                        <button type="submit">
                            Отправить
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-form__layout app-form__layout--img" data-section-bg="<?=$pictureNearForm?>"><img
                        src="<?=$pictureNearForm?>" alt=""></div>
        </form>
        <!-- b:app-form -->
    </main>
</div>
<div class="app-body__fake-footer"></div>
<script src="script.js"></script>
<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>
