<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Контакты');
$bi_contacts = new bi_contacts();
$contacts = $bi_contacts->getContacts();
?>
<div class="app-body">
    <main class="app-content">
        <div class="contacts">
            <div class="contacts__inner">
                <div class="h1 contacts__title"> Контакты</div>
                <div class="contacts__text">
                    <?$APPLICATION->IncludeFile(
                        '/local/templates/BI_apk/include_areas/contacts/title_text.php',
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </div>
            </div>
        </div>
        <!-- b:contacts -->
        <div class="map">
            <div class="map__layout map__layout--content">
                <div class="map__content">
                    <div class="map__title"> <span>
    						<?=$contacts['NAME']?>
    					</span></div>
                    <div class="map__contacts">
                        <div class="map__contacts-block">
                            <div class="map__contacts-title"> Юридический адрес:</div>
                            <div class="map__contacts-item">
                                <div class="map__contacts-item-text"><span class="svg-icon svg-icon--address"><svg
                                                class="svg-icon__link"><use xlink:href="#address"/></svg></span>
                                    <?=$contacts['PROPS']['ADDRESS']['~VALUE']?>
                                </div>
                            </div>
                        </div>
                        <div class="map__contacts-block">
                            <div class="map__contacts-title"> Телефоны:</div>
                            <?foreach ($contacts['PROPS']['PHONE']['VALUE'] as $ph){?>
                            <div class="map__contacts-item">
                                <div class="map__contacts-item-icon"><span class="svg-icon svg-icon--phone"><svg
                                                class="svg-icon__link"><use xlink:href="#phone"/></svg></span></div>
                                <div class="map__contacts-item-text"><a href="tel:+79608551315"><?=$ph?></a>
                                </div>
                            </div>
                            <?}?>
                        </div>
                        <div class="map__contacts-block">
                            <div class="map__contacts-title"> E-mail:</div>
                            <?foreach ($contacts['PROPS']['EMAIL']['VALUE'] as $email){?>
                            <div class="map__contacts-item">
                                <div class="map__contacts-item-icon"><span class="svg-icon svg-icon--email"><svg
                                                class="svg-icon__link"><use xlink:href="#email"/></svg></span></div>
                                <div class="map__contacts-item-text"><a href="email:<?=$email?>"><?=$email?></a>
                                </div>
                            </div>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="map__layout map__layout--block">
                <div class="map__block" id="map"></div>
            </div>
        </div>
        <!-- b:map -->
        <form class="app-form app-form--contacts">
            <div class="app-form__layout app-form__layout--form">
                <div class="app-form__block" >
                    <div class="app-form__title"> Напишите нам</div>
                    <div class="app-form__item app-form__item--input">
                        <div class="app-form__item-control"><input type="text" name="name" placeholder="Ваше имя"></div>
                    </div>
                    <div class="app-form__item app-form__item--select">
                        <div class="app-form__item-control">
                            <select name="theme">
                                <option selected value="Без темы">Тема письма</option>
                                <option value="Жалоба">
                                    Жалоба
                                </option>
                                <option value="Предложение">
                                    Предложение
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="app-form__item app-form__item--input">
                        <div class="app-form__item-control"><input type="email" name="email" required placeholder="E-mail"></div>
                    </div>
                    <div class="app-form__item app-form__item--textarea">
                        <div class="app-form__item-control"><textarea name="message" placeholder="Ваше сообщение"></textarea></div>
                    </div>
                    <div class="app-form__item app-form__item--agreement">
                        <div class="app-form__item-control"><input checked type="checkbox" required id="form-checkbox1">
                        </div>
                        <div class="app-form__item-label"><label for="form-checkbox1">Я соглашаюсь на обработку <a
                                        href="/public-offer/">персональных данных</a></label></div>
                    </div>
                    <div class="app-form__item app-form__item--button">
                        <button type="submit">
                            Отправить
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-form__layout app-form__layout--img" data-section-bg="<?=SITE_TEMPLATE_PATH?>/images/files/form-bg1.png">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/files/form-bg1.png" alt=""></div>
        </form>
        <!-- b:app-form -->
    </main>
</div>
<div class="app-body__fake-footer"></div>
<script src="script.js"></script>
<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>
