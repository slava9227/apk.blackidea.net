/************************************************
 *                  События
 ***********************************************/
$(function () {

    // отправка контактной формы
    $('.app-form.app-form--contacts').submit(function (e) {
        e.preventDefault();
        var $form = $(this);

        $.ajax({
            type: 'POST',
            url: '/ajax/forms/contacts_form.php',
            data: $form.serialize(),
            success: function (mes) {
                if(mes === 'ok'){
                    alert('Сообщение доставлено!');
                    $form[0].reset();
                }else {
                    console.error('Произошла ошибка отправки формы. Обратитесь к администратору сайта.')
                }
            },
            error:function (mes) {
                console.error('Произошла ошибка: ');
                console.error(mes);
            }
        });
    });


});

