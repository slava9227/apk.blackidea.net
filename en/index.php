<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('Main');

// news
$bi_news = new bi_news();
$lastNews = $bi_news->get_news(['IBLOCK_ID' => 25, 'PROPERTY_PROP_SHOW_ON_MAIN_VALUE' => 'Yes'], 1, true);
$mediaObj = new bi_data();
?>

    <div class="app-body">
        <main class="app-content">
            <div class="main-banner">
                <div class="main-banner__background" data-section-bg="<?=SITE_TEMPLATE_PATH?>/images/img/main-banner.png">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/img/main-banner.png" alt=""></div>
                <div class="main-banner__inner">
                    <div class="main-banner__title">
                        <?$APPLICATION->IncludeFile(
                            '/local/templates/BI_apk/include_areas/en/main/first_tittle.php',
                            Array(),
                            Array("MODE"=>"html")
                        );?>
                    </div>
                    <div class="main-banner__description">
                        <?$APPLICATION->IncludeFile(
                            '/local/templates/BI_apk/include_areas/en/main/second_tittle.php',
                            Array(),
                            Array("MODE"=>"html")
                        );?>
                    </div>
                    <article class="news-item">
                        <div class="news-item__square">
                            <div class="news-item__square-item news-item__square-item--first"></div>
                            <div class="news-item__square-item news-item__square-item--second"></div>
                            <div class="news-item__square-item news-item__square-item--third"></div>
                            <div class="news-item__square-item news-item__square-item--fourth"></div>
                        </div>
                        <h3 class="news-item__title"><?=$lastNews['NAME']?></h3>
                        <div class="news-item__text"><?=$lastNews['PREVIEW_TEXT']?></div>
                        <div class="news-item__link"><a href="/news/<?=$lastNews['CODE']?>">
                                Read more
                            </a></div>
                    </article>
                    <!-- b:news-item -->
                    <div class="main-banner__arrow"><span class="svg-icon svg-icon--arrow"><svg class="svg-icon__link"><use
                                    xlink:href="#arrow"/></svg></span></div>
                </div>
            </div>
            <!-- b:main-banner -->
            <div class="main-slider">
                <div class="main-slider__items">

                    <?$APPLICATION->IncludeComponent("bitrix:news.list", "slider_main", Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                        "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                        "AJAX_MODE" => "N",	// Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                        "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                        "CACHE_TYPE" => "A",	// Тип кеширования
                        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                        "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                        "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                        "DISPLAY_NAME" => "Y",	// Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                        "FIELD_CODE" => array(	// Поля
                            0 => "",
                            1 => "NAME",
                            2 => "DETAIL_TEXT",
                            3 => "DETAIL_PICTURE",
                            4 => "",
                        ),
                        "FILTER_NAME" => "",	// Фильтр
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "1",	// Код информационного блока
                        "IBLOCK_TYPE" => "index_slider",	// Тип информационного блока (используется только для проверки)
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                        "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                        "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                        "NEWS_COUNT" => "20",	// Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                        "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости",	// Название категорий
                        "PARENT_SECTION" => "",	// ID раздела
                        "PARENT_SECTION_CODE" => "",	// Код раздела
                        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                        "PROPERTY_CODE" => array(	// Свойства
                            0 => "",
                            1 => "",
                        ),
                        "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
                        "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                        "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
                        "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
                        "SET_STATUS_404" => "N",	// Устанавливать статус 404
                        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                        "SHOW_404" => "N",	// Показ специальной страницы
                        "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                        "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
                    ),
                        false
                    );?>
                </div>
                <div class="main-slider__arrow"><span class="svg-icon svg-icon--arrow"><svg class="svg-icon__link"><use
                                xlink:href="#arrow"/></svg></span></div>
                <div class="main-slider__line">
                    <div class="main-slider__line-item"></div>
                </div>
            </div>
            <!-- b:main-slider -->
            <div class="about">
                <div class="about__inner">
                    <div class="about__topline">
                        <div class="about__topline-layout about__topline-layout--content">
                            <div class="h1 about__topline-title" data-aos-duration="1000" data-aos="fade-up"> About factory</div>
                        </div>
                        <div class="about__topline-layout about__topline-layout--img">
                            <div class="about__topline-img">
                                <div class="about__topline-img-parallax">
                                    <?$APPLICATION->IncludeFile(
                                        '/local/templates/BI_apk/include_areas/en/main/about_image_block.php',
                                        Array(),
                                        Array("MODE"=>"html")
                                    );?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about__content" data-aos-duration="1000" data-aos="fade-up">
                        <div class="about__content-topline"> <span>
                            <?$APPLICATION->IncludeFile(
                                '/local/templates/BI_apk/include_areas/en/main/about_title_block.php',
                                Array(),
                                Array("MODE"=>"html")
                            );?>
    					</span></div>
                        <div class="about__content-bottom">
                            <?$APPLICATION->IncludeFile(
                                '/local/templates/BI_apk/include_areas/en/main/about_text_block.php',
                                Array(),
                                Array("MODE"=>"html")
                            );?>

                        </div>
                    </div>
                    <div class="about__slider">
                        <div class="scale-slider scale-slider--about">
                            <div class="scale-slider__inner">
                                <?foreach ($mediaObj->get_pictures('main', 2, true) as $slider_item):?>
                                    <div class="scale-slider__item">
                                        <a class="scale-slider__item-img" href="<?=$slider_item['DETAIL_PICTURE']?>">
                                            <img src="<?=$slider_item['PREVIEW_PICTURE']?>" alt="<?=$slider_item['NAME']?>">
                                        </a>
                                    <div class="scale-slider__item-content"> <?=$slider_item['PREVIEW_TEXT']?></div>
                                </div>
                                <?endforeach;?>
                            </div>
                        </div>
                        <!-- b:scale-slider -->
                    </div>
                </div>
            </div>
            <!-- b:about -->
        </main>
    </div>
    <div class="app-body__fake-footer"></div>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>