<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Производство");
$bi_production = new bi_production();
$productionArr = $bi_production->getProductionTabs();
?>

    <div class="app-body">
        <main class="app-content">
            <div class="production">
                <div class="production__inner">
                    <div class="h1 production__title"> Производство</div>
                    <div class="production__text">
                        <? $APPLICATION->IncludeFile(
                            '/local/templates/BI_apk/include_areas/production/title_text.php',
                            Array(),
                            Array("MODE" => "html")
                        ); ?>
                    </div>
                </div>
                <div class="production__background"
                     data-section-bg="<?= SITE_TEMPLATE_PATH ?>/images/files/production-bg.jpg"><img
                            src="<?= SITE_TEMPLATE_PATH ?>/images/files/production-bg.jpg" alt=""></div>
            </div>
            <!-- b:production -->
            <div class="tabs">
                <div class="tabs__inner">
                    <div class="tabs__layout tabs__layout--left">
                        <div class="tabs__links">
                            <?$count = 1;?>
                            <?foreach ($productionArr as $item) {?>
                                <div class="tabs__link" data-hash="<?=$item['CODE']?>">
                                    <div class="tabs__link-icon"><span class="svg-icon svg-icon--tabs<?=$count?>">

                                        <object class="svg-icon__link" type="image/svg+xml" data="<?=$item['SVG']?>" ></object>
                                        </span></div>
                                    <div class="tabs__link-title"> <?=$item['NAME']?></div>
                                </div>
                            <?$count++;?>
                            <?}?>

                        </div>
                        <div class="tabs__images">
                            <?foreach ($productionArr as $item) {?>
                                <div class="tabs__images-item">
                                    <div class="tabs__images-inner">
                                        <div class="tabs__images-block" data-stellar-ratio="0.5" alt="">
                                            <img src="<?=$item['DETAIL_PICTURE']?>">
                                            <div class="tabs__images-content"> <span class="tabs__images-title">
                                            3000
                                        </span> <span class="tabs__images-description">
                                            Га
                                        </span></div>
                                        </div>
                                    </div>
                                </div>
                            <?}?>
                        </div>
                    </div>
                    <div class="tabs__layout tabs__layout--right">
                        <div class="tabs__content">
                            <?foreach ($productionArr as $item) {?>
                                <div class="tabs__content-item">
                                    <div class="tabs__content-item-inner">
                                        <div class="tabs__content-item-title">
                                            <span><?=$item['PREVIEW_TEXT']?></span></div>
                                        <div class="tabs__content-item-text">
                                            <?=$item['DETAIL_TEXT']?>
                                        </div>
                                    </div>
                                </div>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- b:tabs -->
        </main>
    </div>
    <div class="app-body__fake-footer"></div>
    <script src="script.js"></script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>